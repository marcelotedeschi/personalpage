<!DOCTYPE HTML>
<html>
	<head>
		<title>Marcelo T</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<script src="js/jquery.min.js"></script>
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<script src="https://www.google.com/recaptcha/api.js" async defer></script>
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-mobile.css" />
			<link rel="stylesheet" href="css/style-desktop.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body>

		<!-- Wrapper-->
			<div id="wrapper">

				<!-- Nav -->
					<nav id="nav">
						<a href="#me" class="icon fa-home active"><span>Home</span></a>
						<a href="#work" class="icon fa-folder"><span>Portfolio</span></a>
						<a href="#contact" class="icon fa-envelope"><span>Contato</span></a>
						<a href="http://br.linkedin.com/in/marcelotedeschi" class="icon fa-linkedin" target="_blank"><span>LinkedIn</span></a>
					</nav>

				<!-- Main -->
					<div id="main">

						<!-- Me -->
							<article id="me" class="panel">
								<header>
									<h1>Marcelo Tedeschi</h1>
								</header>
								<a href="#work" class="jumplink pic">
									<span class="arrow icon fa-chevron-right"><span>Veja meu trabalho</span></span>
									<img src="images/me.jpg" alt="" />
								</a>
							</article>

						<!-- Work -->
							<article id="work" class="panel">
								<header>
									<h2>Portfolio</h2>
								</header>
								<p>
									Abaixo estão alguns de meus projetos. Não hesite em explorá-los!
								</p>
								<section>
									Sites
									<div class="row">
										<div class="4u">
											<a href="http://www.enactus.ufscar.br" class="image fit" target="_blank"><img src="images/enactus.jpg" alt=""></a>
										</div>
										<div class="4u">
											<a href="http://www.heldertedeschi.com" class="image fit" target="_blank"><img src="images/ht.jpg" alt=""></a>
										</div>
										<div class="4u">
											<a href="#" class="image fit" target="_blank"><img src="images/construtora.jpg" alt=""></a>
										</div>
										<div class="4u">
											<a href="http://www.simposiodemencia.ufscar.br" class="image fit" target="_blank"><img src="images/simposio.jpg" alt=""></a>
										</div>
									</div>
									<br>
									iOS Apps
									<div class="row">
										<div class="4u">
											<a href="https://marvelapp.com/125f444" class="image fit" target="_blank"><img class="ios" src="images/boxee.jpg" alt=""></a>
										</div>
										<div class="4u">
											<a href="https://marvelapp.com/55feb0" class="image fit" target="_blank"><img class="ios" src="images/petiko.jpg" alt=""></a>
										</div>
									</div>
								</section>
							</article>

						<!-- Contact -->
							<article id="contact" class="panel">
								<header>
									<h2>Contato</h2>
								</header>
								<!-- Codigo php para envio do email -->
								<?php
									function spamcheck($field) {
									  // Sanitize e-mail address
									  $field=filter_var($field, FILTER_SANITIZE_EMAIL);
									  // Validate e-mail address
									  if(filter_var($field, FILTER_VALIDATE_EMAIL)) {
									    return TRUE;
									  } else {
									    return FALSE;
									  }
									}

									// if submit is clicked
									if (isset($_POST["email"])) {
										$captcha=$_POST['g-recaptcha-response'];

										if(!$captcha){
											?>
											<h3>Houve um erro na mensagem</h3>
											<p>Favor clicar no reCaptcha.</p>
											<?php
										} else {

											$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LcFPiEUAAAAAIAk7DXhAjUXbWqPiiiEWi6K2mvR&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
											if ($response.success==false) {
												?>
												<h3>Houve um erro na mensagem</h3>
												<p>Favor clicar no reCaptcha.</p>
												<?php
											} else {
												$from = $_POST["email"]; // sender
												$name = $_POST["name"];
												$subject = "Site Pessoal MT - Assunto: " . $_POST["subject"];
												$message = " Nome: " . $name . "\n Mensagem: " . $_POST["message"];
												// message lines should not exceed 70 characters (PHP rule), so wrap it
												$message = wordwrap($message, 70);
												// Check if "from" email address is valid
												$mailcheck = spamcheck($_POST["email"]);
												if ($mailcheck==FALSE) {?>
													<h3>Houve um erro na mensagem</h3>
													<p>Tente novamente, por favor. É possível que seu email tenha sido digitado incorretamente.</p><?php
												} else {
													// send mail
													mail("marceloftedeschi@gmail.com",$subject,$message,"From: $from\n");?>
													<h3>Seu email foi enviado com sucesso!</h3>
													<p>Fique a vontade para enviar uma nova mensagem se desejar.</p><?php
												}
											}
										}
									}
									?>
								<form action="#contact" method="post">
									<div>
										<div class="row">
											<div class="6u">
												<input type="text" name="name" placeholder="Nome" />
											</div>
											<div class="6u">
												<input type="text" name="email" placeholder="Email" />
											</div>
										</div>
										<div class="row">
											<div class="12u">
												<input type="text" name="subject" placeholder="Assunto" />
											</div>
										</div>
										<div class="row">
											<div class="12u">
												<textarea name="message" placeholder="Mensagem" rows="8"></textarea>
											</div>
										</div>
										<div class="row">
											<div class="12u">
												<div class="g-recaptcha" data-sitekey="6LcFPiEUAAAAAFCcYPZDtSDnJ791PlZ38ECFYa8h"></div>
											</div>
										</div>
										<div class="row">
											<div class="12u">
												<input type="submit" value="Enviar Mensagem" />
											</div>
										</div>
									</div>
								</form>
							</article>
					</div>

				<!-- Footer -->
					<div id="footer">
						<ul class="copyright">
							<li>&copy; Todos os direitos reservados</li><li>Design: <a href="mailto:marceloftedeschi@gmail.com">Marcelo T</a></li><li>Website in <a href="index-en.php">English</a></li>
						</ul>
					</div>

			</div>

	</body>
</html>
